import Vue from 'vue';
import Dashboard from '@/components/Dashboard';

describe('Dashboard.vue', () => {
  it('should render search box correct contents', () => {
    const Constructor = Vue.extend(Dashboard);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.search-input').placeholder)
      .toEqual('Search for a challenge');
  });

  it('should render 3 filter options', () => {
    const Constructor = Vue.extend(Dashboard);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelectorAll('.data-filter ons-col').length)
      .toBe(3);
  });
});
